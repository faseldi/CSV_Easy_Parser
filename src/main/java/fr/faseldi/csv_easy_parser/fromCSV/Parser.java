package fr.faseldi.csv_easy_parser.fromCSV;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.IntStream;

/**
 *
 * @author Faseldi - Florian Duneau
 */
public class Parser {
    /**
     * row and columns of the file
     */
    private List<String[]> rows; 
    /**
     * 
     * @param separator the separator used to parse the csv
     * @param file the file path
     */
    public Parser(String separator, String file) {
        try {
            parse(separator, file);
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    /**
     * default separator ;
     * @param file the file path
     */
    public Parser(String file){
        try {
            parse(";", file);
        }catch(IOException e){
            
        }
    }
    public List<String[]> getRows() {
        return rows;
    }
    private void parse(String separator, String file) throws IOException{
        rows = new ArrayList<>();
        Files.lines( Paths.get(file) )
             .filter(line -> line.contains(separator))
             .forEach( line -> {
                String[] spl = line.split( separator );
                Predicate<String> pred = (s) -> s.length() > 1 && String.valueOf(s.charAt(0)).equals("\"") && String.valueOf(s.charAt( s.length() -1)).equals("\"");
                if(Arrays.asList(spl).stream().allMatch( pred )){
                    // if format is "value"
                    IntStream.range(0, spl.length)
                             .forEach( i -> {
                                 spl[i] = spl[i].isEmpty() ? "" : spl[i].substring(1, spl[i].length() - 1);
                             });
                }
                rows.add( spl );
             });
    }
}

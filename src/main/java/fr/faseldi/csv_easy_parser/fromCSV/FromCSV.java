package fr.faseldi.csv_easy_parser.fromCSV;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 *
 * @author Faseldi
 */
public class FromCSV {
    private static <T extends Object> T rowToObject(String[] row, String[] colNames, Class<T> clazz) {
        final T o;
        try{
            Class<?> claz = Class.forName(clazz.getName());
            if(claz.getDeclaredConstructors().length >= 1){
                Constructor c = claz.getDeclaredConstructors()[0];
                c.setAccessible(true);
                o = (T) c.newInstance();
            }else{
                o = clazz.newInstance();
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
        IntStream.range(0, row.length)
                 .forEach( column -> {
                    try{
                        Field f;
                        try{
                            f = clazz.getDeclaredField(colNames[column]);
                        }catch(NoSuchFieldException e1){
                            f = clazz.getDeclaredFields()[column];
                        }
                        f.setAccessible(true);
                        setValue(o, f, row[column]);
                    }catch(SecurityException | IllegalAccessException e){
                        e.printStackTrace();
                    }
                 });
        return o;
    }
    private static void setValue(Object o, Field field, Object value) throws IllegalArgumentException, IllegalAccessException{
        String type = field.getType().getTypeName();
        field.setAccessible(true);
        try{
           switch ( type ){
                case "int" : field.setInt(o, Integer.valueOf(value.toString()));
                            break;
                case "boolean" : field.setBoolean(o, Boolean.getBoolean(value.toString()));
                            break;
                case "byte" : field.setByte(o, Byte.valueOf(value.toString()));
                            break;
                case "double" : field.setDouble(o, Double.valueOf(value.toString()));
                            break;
                case "float" : field.setFloat(o, Float.valueOf(value.toString()));
                            break;
                case "long" : field.setLong(o, Long.valueOf(value.toString()));
                            break;
                case "short" : field.setShort(o, Short.valueOf(value.toString()));
                            break;
                case "char" : field.setChar(o, value.toString().charAt(0));
                            break;
                default:
                    field.set(o, value);
                    break;
           }
        }catch(ClassCastException | NumberFormatException e){
            try{
                if(field.getType().isPrimitive()){
                    field.set(o, 0);
                }else{
                    field.set(o, field.getType().newInstance());
                }
            }catch(InstantiationException e1){
                field.set(o, null);
            }
        }
    }
    public static <T extends Object> T dump(Parser parser, Class<T> clazz) {
        List<String[]> rows = parser.getRows();
        List<String> colNames = new ArrayList<>();
        colNames.addAll(Arrays.asList(rows.get(0)));
        Field[] fields = clazz.getComponentType().getDeclaredFields();
        Arrays.stream(fields)
              .forEach( f ->{
                    if(!colNames.contains(f.getName())){
                        colNames.clear();
                        Arrays.stream(fields)
                              .forEach( field -> {
                                colNames.add(field.getName());
                              });
                    }  
              });
        if(clazz.isArray()){
            Object arr = Array.newInstance( clazz.getComponentType(), rows.size() -  1 );
            IntStream.range(1, rows.size())
                     .forEach( columns -> {
                         Object o = rowToObject(rows.get(columns), colNames.toArray( new String[]{} ), clazz.getComponentType());
                         Array.set(arr, columns-1, o);
                     });
            return (T) arr;
        }else{
            return null;
        }
    }    
}

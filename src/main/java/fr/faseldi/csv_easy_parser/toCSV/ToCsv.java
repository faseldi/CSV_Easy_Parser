/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.faseldi.csv_easy_parser.toCSV;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Faseldi
 */
public class ToCsv {
    public static String arrayToCSV(Object[] list, String separator){
        String csv = "";
        for(Object o : list){
            if(o == null){
                continue;
            }
            List<String> line = new ArrayList<>();
            Arrays.asList( o.getClass().getMethods() )
                  .forEach( m -> {
                      String[] spl = m.getName().split("\\.");
                      String simpleName = spl[spl.length - 1];
                      // if its a getter and it doesn't return void
                      if(m.getParameterCount() == 0 && !m.getReturnType().getName().equals("void") && simpleName.length() >= 3 && simpleName.substring(0, 3).equals("get") && !simpleName.equals("getClass")){
                          m.setAccessible(true);
                          try{
                            line.add(m.invoke(o).toString());
                          }catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e){
                              e.printStackTrace();
                          }
                      }
                  });
            csv += line.stream().collect(Collectors.joining(separator)) + "\n";
        }
        return csv;
    }
}

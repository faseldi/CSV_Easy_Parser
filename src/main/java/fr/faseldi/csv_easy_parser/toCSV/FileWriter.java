package fr.faseldi.csv_easy_parser.toCSV;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 *
 * @author Faseldi
 */
public class FileWriter {
    public static boolean createCsv(String csv, String filePath){
        try{
            Files.write(Paths.get(filePath), Arrays.asList(csv.split("\n")));
            return true;
        }catch(IOException e){
            return false;
        }
    }
}

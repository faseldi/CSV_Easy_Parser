package fr.faseldi.csv_easy_parser;

import fr.faseldi.csv_easy_parser.fromCSV.FromCSV;
import fr.faseldi.csv_easy_parser.fromCSV.Parser;
import fr.faseldi.csv_easy_parser.toCSV.FileWriter;
import fr.faseldi.csv_easy_parser.toCSV.ToCsv;
import java.util.List;
/**
 *
 * @author Faseldi - Florian Duneau
 */
public class EasyCSV {
    /**
     * 
     * @param <T> the type, that needs to extends object
     * @param file the file path you want to load
     * @param separator the separator string
     * @param clazz the array class such as MyClass[].class
     * @return an array class instance with the loaded csv
     */
    public static <T extends Object> T fromCsv(String file, String separator, Class<T> clazz) {
        Parser parser = new Parser(separator, file);
        return FromCSV.dump(parser, clazz);
    }
    /**
     * 
     * @param <T> the type, that needs to extends object
     * @param file the file path you want to load
     * @param clazz the array class such as MyClass[].class
     * @return an array class instance with the loaded csv
     * default separator is ;
     */
    public static <T extends Object> T fromCsv(String file, Class<T> clazz) {
        Parser parser = new Parser(file);
        return FromCSV.dump(parser, clazz);
    }
    /**
     * 
     * @param list the list you want to convert
     * @return the string representation of your list attributes accessible thaks to getters
     */
    public static String toCsv(List<Object> list){
        return ToCsv.arrayToCSV(list.toArray(), ";");
    }
    /**
     * 
     * @param list the list you want to convert
     * @return the string representation of your list attributes accessible thaks to getters
     */
    public static String toCsv(Object[] list){
        return ToCsv.arrayToCSV(list, ";");
    }
    /**
     * 
     * @param list the list you want to convert
     * @param separator the separator you want to use
     * @return the string representation of your list attributes accessible thaks to getters
     */
    public static String toCsv(Object[] list, String separator){
        return ToCsv.arrayToCSV(list, separator);
    }
     /**
     * 
     * @param list the list you want to convert
     * @param separator the separator you want to use
     * @return the string representation of your list attributes accessible thaks to getters
     */
    public static String toCsv(List<Object> list, String separator){
        return ToCsv.arrayToCSV(list.toArray(), separator);
    }
    /**
     * 
     * @param csv the string csv
     * @param filePath the filePath you want to wriite
     * @return a boolean that indicates if the file qs been created
     */
    public static boolean writeCSV(String csv, String filePath){
        return FileWriter.createCsv(csv, filePath);
    }
}

package fr.faseldi.csv_easy_parser;

import java.io.File;
import java.util.Arrays;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Faseldi
 */
public class CSVTestToCSV {
    
    public CSVTestToCSV() {
    }

    @Test
    public void testTo() {
        SomeThings[] some = new SomeThings[2];
        some[0] = new SomeThings(10,"str",10.2f);
        some[1] = new SomeThings(42,"str2",8.3f);
        String need = "10;str;10.2\n";
        need += "42;str2;8.3\n";
        Assert.assertEquals(need, EasyCSV.toCsv(some));
        Assert.assertEquals(need, EasyCSV.toCsv( Arrays.asList(some)) );
    }
    @Test
    public void testWrite() {
        SomeThings[] some = new SomeThings[2];
        some[0] = new SomeThings(10,"str",10.2f);
        some[1] = new SomeThings(42,"str2",8.3f);
        EasyCSV.writeCSV( EasyCSV.toCsv(some), "src/test/ressources/testWrite.csv");
        Assert.assertTrue(new File("src/test/ressources/testWrite.csv").exists());
    }
    static class SomeThings{
        private int i;
        private String s;
        private float f;
        public SomeThings(int i, String s, float f) {
            this.i=i;
            this.s=s;
            this.f=f;
        }
        public int getI(){
            return i;
        }
        public String getS(){
            return s;
        }
        public float getF(){
            return f;
        }
    }
}

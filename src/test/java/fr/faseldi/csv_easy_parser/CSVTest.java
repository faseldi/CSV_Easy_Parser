package fr.faseldi.csv_easy_parser;


import fr.faseldi.csv_easy_parser.EasyCSV;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Faseldi
 */
public class CSVTest {
    

    @Test
    public void testLoadTypes() {
        Dispositif[] disp = EasyCSV.fromCsv("src/test/ressources/dispositif.csv", ",", Dispositif[].class);
        for(Dispositif d : disp){
            Assert.assertTrue(d.idD == (byte) d.idD);
            Assert.assertTrue(d.nomD.getClass().getTypeName().equals(String.class.getTypeName()));
            Assert.assertTrue(d.typeD.getClass().getTypeName().equals(String.class.getTypeName()));
            Assert.assertTrue(d.posX == (int) d.posX);
            Assert.assertTrue(d.posY == (long) d.posY);
            Assert.assertTrue(d.posZ == (float) d.posZ);
            Assert.assertTrue(d.lieu.getClass().getTypeName().equals(String.class.getTypeName()));
        }
    }
    @Test
    public void testLoadTypesNoNames() {
        Dispositif[] disp = EasyCSV.fromCsv("src/test/ressources/dispositifNoColNames.csv", ",", Dispositif[].class);
        for(Dispositif d : disp){
            Assert.assertTrue(d.idD == (byte) d.idD);
            Assert.assertTrue(d.nomD.getClass().getTypeName().equals(String.class.getTypeName()));
            Assert.assertTrue(d.typeD.getClass().getTypeName().equals(String.class.getTypeName()));
            Assert.assertTrue(d.posX == (int) d.posX);
            Assert.assertTrue(d.posY == (long) d.posY);
            Assert.assertTrue(d.posZ == (float) d.posZ);
            Assert.assertTrue(d.lieu.getClass().getTypeName().equals(String.class.getTypeName()));
        }
    }
    static class Dispositif {
        byte idD;
        String nomD;
        String typeD;
        String lieu;
        int posX;
        long posY;
        float posZ;
        @Override
        public String toString(){
            return String.format("%d %s %s %s %d %d %.2f", 
                    idD, nomD, typeD, lieu, posX, posY, posZ);
        }
    }
}

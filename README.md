# CSV_Easy_Parser : The simpliest way to load your CSV

## Introduction
> The goal of this project is to suit as simple as the GSon's google API to load GSon.
> By searching the same thing to load csv on internet, I haven't found any result.
> So, I decided to create this API wich works with relexivity to suits as easiest as possible to use.

## Requirements
    You only need Java 8 (uses of stream and lambda)
    
## Releases

[1.0 On google drive](https://drive.google.com/file/d/0BwcZ1OkGdsOuV2h5RFZpaFczeXM/view?usp=sharing)

Features : 

    Load CSV File into an Array of a specified Object : MyObject[].class
    
    Create CSV from Array (as String) and write it on a file
    
[1.1 On drive](https://drive.google.com/open?id=0BwcZ1OkGdsOuV2h5RFZpaFczeXM)

Feature :

    Load an invalid format value in a field now set null or 0 (if primitive)
    
## How to use
### Load your CSV
    You have a MySQL Table Person exported as CSV to load.
    The Columns looks like this
    id | name | surname | age ...
    Integer | String | String | Short
    You want to load an export CSV of this table.
    Just create the corresponding class :
    
        static class Person {
            int id;
            String name;
            String surname;
            short age;
        }
    
    and to load the data, you simply have to write the following code :
    
        Person[] people = EasyCSV.fromCsv("path_to_your_csv", Person[].class);
        
    The default sseparator is ;
    You can specify the one you want using :
        EasyCSV.fromCsv("path_to_your_csv", "my_separator", Person[].class);
    
    It will automatically create the array with the data into the right attributes.
    However, if the first row of your csv isn't the column names, you have to keep the right order in your attributes declaration.
## Create your CSV
    To create a CSV you can simply use the toCSV method as follows :
    EasyCSV.toCSV(new String[]{"b","é","p","o"});
    or use a separator :
    EasyCSV.toCSV(new String[]{"b","é","p","o"}, ",");
    Default separator is ";"
    To write your CSV into a file :
    EasyCSV.writeCSV( EasyCSV.toCsv(someObjectArray), "myfile.csv");
    This method return a boolean that indicated if an exception occurs while creating the file.
## Thanks - Contribute
  * I developed this project on my own.. (Florian Duneau).
  * You are free to re-use it has you want.
  * You are free to upgrade this project. You are no obligated to difse your modifications, but I'll be happy to merge them.
